runit-services (0.4.0) unstable; urgency=low

  * sv/cron/run: no longer apply memory limit to cron (amd64 requires
    more than 11000000, thx Joost van Baal).
  * debian/rules: minor.
  * debian/control: Standards-Version: 3.8.0.1.

 -- Gerrit Pape <pape@smarden.org>  Tue, 17 Jun 2008 19:48:39 +0000

runit-services (0.3.2) unstable; urgency=low

  * debian/copyright: 2008.
  * debian/implicit: add proper dependencies to support 'parallel build'
    through make -j (thx Daniel Schepler for the patch).
  * README, sv/cron/run, sv/nfs-kernel-server/finish,
    debian/runit-services.prerm.in: use runit's update-service program to
    check/add/remove services, instead of dealing with symlinks in
    /var/service/ directly.
  * debian/control: Depends: runit (>= 1.8.0-2) (1st version that provides
    the update-service program).
  * debian/control: Standards-Version: 3.7.3.0.

 -- Gerrit Pape <pape@smarden.org>  Sun, 17 Feb 2008 17:10:12 +0000

runit-services (0.3.1) unstable; urgency=low

  * debian/control: move Recommends: dash, socklog-run to Suggests: (thx
    Daniel Kahn Gillmor, closes: #450808).

 -- Gerrit Pape <pape@smarden.org>  Mon, 10 Dec 2007 09:38:12 +0000

runit-services (0.3.0) unstable; urgency=low

  * debian/implicit: update to revision 1.11.
  * sv/ssh/run: make sure privilege separation directory /var/run/sshd
    exists (thx Daniel Kahn Gillmor for the patch, closes: #421398).
  * sv/rpc.statd/: new: service directory for rpc.statd (thx Daniel Kahn
    Gillmor for the patch, closes: #421401).

 -- Gerrit Pape <pape@smarden.org>  Sun, 06 Apr 2007 14:41:16 +0000

runit-services (0.2.3) unstable; urgency=low

  * sv/chrony: new; add chrony service directory.

 -- Gerrit Pape <pape@smarden.org>  Thu, 29 Jun 2006 08:23:18 +0000

runit-services (0.2.2) unstable; urgency=low

  * sv/portmap/run: use -f option to portmap, instead of -d.
  * debian/copyright: 2006.
  * debian/control: Standards-Version: 3.7.2.0.
  * debian/implicit: update to revision 1.11.

 -- Gerrit Pape <pape@smarden.org>  Sat, 27 May 2006 16:21:33 +0000

runit-services (0.2.1) unstable; urgency=low

  * sv/ssh/run: don't limit memory to ssh daemon by default (closes:
    #352959).
  * sv/dhcp/run: add $INTERFACES to arguments to dhcpd.

 -- Gerrit Pape <pape@smarden.org>  Sun, 26 Feb 2006 13:10:44 +0000

runit-services (0.2.0) unstable; urgency=low

  * sv/ssh/log/run: don't use "$_" in run script (patch from Joshua N
    Pritikin, closes: #350918).
  * debian/rules: create ./supervise/ subdirectory symlinks instead of
    including them in .tar.gz; symlink /var/log/<service>/ if exists to
    /etc/sv/<service>/log/main/.
  * sv/dhclient/log/run: don't use "$_" in run script.
  * debian/runit-services.prerm.in: new; shutdown enabled services on
    package removal.
  * debian/rules: create debian/runit-services.prerm from
    debian/runit-services.prerm.in.
  * debian/implicit: update to revision 1.11.

  * sv/portmap: new; add portmap service directory.
  * sv/nfs-kernel-server: new; add nfs-kernel-server services directory.
  * sv/dhcp: new; add dhcp service directory.

 -- Gerrit Pape <pape@smarden.org>  Mon,  6 Feb 2006 17:49:07 +0000

runit-services (0.1.0) unstable; urgency=low

  * initial version (closes: #330029).

 -- Gerrit Pape <pape@smarden.org>  Wed, 28 Dec 2005 10:33:01 +0000
